//
//  HourlyWeatherViewController.swift
//  WeatherApp
//
//  Created by funssoftware on 2/12/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit

class tableHourlyCell : UITableViewCell {
    
    @IBOutlet var hoursLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    
}

class HourlyWeatherViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var cityName: UILabel!
    @IBOutlet var hourlyTable: UITableView!
    
    let _arrays = Arrays()
    var cityId : Int = 0
    var weatherDate : String = ""
    var cityTitle : String = ""
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;

        _arrays.weatherHourlyData = Connector().getHourlyForCity(city:cityId, date:weatherDate)
        _arrays.weatherHourlyData.sort{$0.time < $1.time}
        
        cityName.text = cityTitle+", "+weatherDate

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return _arrays.weatherHourlyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = hourlyTable.dequeueReusableCell(withIdentifier: "tableHourlyCell") as! tableHourlyCell
        cell.hoursLabel.text = _arrays.weatherHourlyData[indexPath.row].time
        cell.tempLabel.text = String(describing:_arrays.weatherHourlyData[indexPath.row].temp)+"°"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hourlyTable.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    


}
