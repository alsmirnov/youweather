//
//  SearchViewController.swift
//  WeatherApp
//
//  Created by funssoftware on 2/6/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit

class searchTableCell:UITableViewCell {
    
    @IBOutlet var searchResLabel: UILabel!
    
}

class SearchViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var searchTable: UITableView!
    @IBOutlet var searchField: UITextField!
    
    let _arrays = Arrays()
    var cityTitleUnwind : String = ""
    var cityIdUnwind : String = ""

    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchField.addDoneButtonOnKeyboard()
        searchField.becomeFirstResponder() // focus on load
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self._arrays.foundCityNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = searchTable.dequeueReusableCell(withIdentifier: "searchTableCell") as! searchTableCell
        cell.searchResLabel.text = _arrays.foundCityNames[indexPath.row].name!
        
        if !_arrays.foundCityNames[indexPath.row].country.isEmpty {
            cell.searchResLabel.text = cell.searchResLabel.text!+", "+_arrays.foundCityNames[indexPath.row].country
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // todo: check if city is already exists in the list
        
        let citiesToWrite = Connector().getCities()+","+self._arrays.foundCityNames[indexPath.row].id!
        
        cityTitleUnwind = self._arrays.foundCityNames[indexPath.row].name!
        cityIdUnwind = self._arrays.foundCityNames[indexPath.row].id!+","
        Connector().saveCities(dataToWrite: citiesToWrite, completion: {
            _ in
            print("going to unwind from search...")
            self.performSegue(withIdentifier: "unwindFromSearch", sender: self)
        })

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    @IBAction func doSearch(_ sender: Any) {
        
        searchField.resignFirstResponder()
        
        // todo: show "no data" if no data
        
        self._arrays.foundCityNames.removeAll()
        self.searchTable.reloadData()
        
        // if search by coordinates (first char is digit, string contains comma)

        if Int(searchField.text!.prefix(1)) != nil && searchField.text!.contains(",") {
            Connector().findCityByCoords(city: searchField.text!, completion:  {
                (data) in
                self._arrays.foundCityNames = data
                DispatchQueue.main.async {
                    self.searchTable.reloadData()
                }
            })
        }
        else {
            Connector().findCityByName(city: searchField.text!, completion:  {
                (data) in
                self._arrays.foundCityNames = data
                DispatchQueue.main.async {
                    self.searchTable.reloadData()
                }
            })
        }
    }

  
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

}
