//
//  Arrays.swift
//  WeatherApp
//
//  Created by funssoftware on 2/8/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit

class Arrays: NSObject {
    
//    var weatherDailyData :[(
//        cityId: Int,
//        cityName: String?,
//        time: String?
//        )] = []
    
    var weatherHourlyData :[(
        cityId: Int,
        city: String,
        time: String,
        temp: String,
        pressure: String,
        humidity: String
    )] = []
    
    var weatherTableData :[(
        cityId: Int,
        city: String,
        temp: Int,
        pressure: Float,
        humidity: Int,
        temp_min: Int,
        temp_max: Int,
        description: String,
        icon: String,
        sky: String
        )] = []
    
    var weatherDailyTableData :[(
        dayOfWeek: String,
        date: String,
        cityId: Int,
        city: String,
        temp: Int,
        pressure: Float,
        humidity: Int,
        temp_min: Int,
        temp_max: Int,
        description: String,
        icon: String,
        sky: String
        )] = []
    
    var foundCityNames :[(
        name:String?,
        country:String,
        id: String?
    )] = []
    
    var weatherDailyData: [(
        cityId: Int,
        cityName: String,
        time: String?,
        day: String,
        date: String,
        temp: String,
        pressure: String,
        humidity: String,
        seaLevel: String,
        sunrise: String,
        sunset: String,
        wind: String
    )] = []
    
    
}
