//
//  DailyWeatherViewController.swift
//  WeatherApp
//
//  Created by funssoftware on 2/9/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit

class tableWeatherCell : UITableViewCell {

    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var dayDateLabel: UILabel!
    @IBOutlet var pressureLabel: UILabel!
    @IBOutlet var humLabel: UILabel!
    @IBOutlet var seaLabel: UILabel!
    @IBOutlet var sunriseLabel: UILabel!
    @IBOutlet var sunsetLabel: UILabel!
    @IBOutlet var windLabel: UILabel!
    @IBOutlet var dayTempLabel: UILabel!

}


class DailyWeatherViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let _arrays = Arrays()

    @IBOutlet var backImage: UIImageView!
    @IBOutlet var weatherTable: UITableView!
    @IBOutlet var cityTitle: UILabel!
    @IBOutlet var tempLabel: UILabel!

    var cityId : Int = 0
    var tempNow : String = ""
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        backImage.downloaded(from: "https://picsum.photos/768/500/?random&blur=true", contentMode: .scaleAspectFill)
        
        Helper().applyShadow(label:cityTitle)
        Helper().applyShadow(label:tempLabel)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;

        _arrays.weatherDailyData = Connector().getDailyForCity(city:cityId)
        

        if(_arrays.weatherDailyData.count > 0) {
            cityTitle.text = _arrays.weatherDailyData[0].cityName
            tempLabel.text = tempNow+"°"
        } else {
            cityTitle.text = "No data yet"
            tempLabel.text = ""
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return _arrays.weatherDailyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = weatherTable.dequeueReusableCell(withIdentifier: "tableWeatherCell") as! tableWeatherCell

        cell.dayTempLabel.text = String(describing:_arrays.weatherDailyData[indexPath.row].temp)+"°"
        cell.pressureLabel.text = String(describing:_arrays.weatherDailyData[indexPath.row].pressure)
        cell.humLabel.text = String(describing:_arrays.weatherDailyData[indexPath.row].humidity)+"%"
        cell.dayLabel.text = _arrays.weatherDailyData[indexPath.row].date
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "hourlyWeatherSegue", sender: self)

        self.weatherTable.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "hourlyWeatherSegue",
            let destination = segue.destination as? HourlyWeatherViewController,
            let selIndex = self.weatherTable.indexPathForSelectedRow?.row
        {
            destination.cityId = _arrays.weatherDailyData[selIndex].cityId
            destination.cityTitle = _arrays.weatherDailyData[selIndex].cityName
            destination.weatherDate = _arrays.weatherDailyData[selIndex].date
        }
    }

}
