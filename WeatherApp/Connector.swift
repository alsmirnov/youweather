//
//  Connector.swift
//  WeatherApp
//
//  Created by funssoftware on 2/8/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit


// did all in one file due a small project; could be split to separate models for each view controller.
class Connector: NSObject {
    
    let _arrays = Arrays()

    let fileManager = FileManager.default
    
    struct mainDec : Decodable {
        let temp     :    Float?
        let pressure :    Float?
        let humidity :    Int?
        let temp_min :    Float?
        let temp_max :    Float?
    }
    
    struct weatherDec : Decodable {
        let id: Int
        let main: String
        let description: String
        let icon: String
    }
    
    struct listDec : Decodable {
        let id: Int
        let main: mainDec
        let name: String
        let weather : [weatherDec]
    }
    
    struct weatherDataDec : Decodable {
        let list : [listDec]
    }
    
    struct searchCity : Decodable {
        let name : String?
        let country : String?
        let city_id : String?
    }

    struct searchCoords : Decodable {
        let name : String?
        let id : Int?
    }
    
//  get weather data from the server and return it as an array
    func getWeatherData(city: String, completion: @escaping (Bool)->()) -> [(
        cityId: Int,
        city: String,
        temp: Int,
        pressure: Float,
        humidity: Int,
        temp_min: Int,
        temp_max: Int,
        description: String,
        icon: String,
        sky: String
        )] {
        
        do{
            let filename = self.getDocumentsDirectory().appendingPathComponent(Settings()._currentWeatherFile)
            let jsonContent = try String(contentsOf: filename);
            let data = Data(jsonContent.utf8)
            
            do {
                let result = try JSONDecoder().decode(weatherDataDec.self, from: data)
                
                if (result == nil){ // if different structure received
                    print("structure doesn't fit 67")
                }
                else{
                    // clearing array to put new data
                    _arrays.weatherTableData.removeAll()
                    
                    for resultData in result.list {

                        _arrays.weatherTableData.append((
                            resultData.id,
                            resultData.name,
                            Int(resultData.main.temp!),
                            resultData.main.pressure!,
                            resultData.main.humidity!,
                            Int(resultData.main.temp_min!),
                            Int(resultData.main.temp_max!),
                            resultData.weather[0].description,
                            resultData.weather[0].icon,
                            resultData.weather[0].main
                        ))
                    }
                }
            }  catch let jsonErr {
                
                print("Error decoding getWeatherData", jsonErr)
                completion(false)
            }
        } catch {
            print("96: file read error")
            completion(false)
            }

            completion(true)
            return _arrays.weatherTableData
    }
    
    
//  get weather for all cities from server
    func getWeatherForCityList(citiesList: String, completion: @escaping (Bool)->()){

            let request = self.getRequest(urlAddress: Settings().MAIN_API_URL+Settings().GET_WEATHER_URL+citiesList+Settings().GET_WEATHER_APPID+Settings().API_KEY, method: "GET", sendParams: "" )
            
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                guard data != nil && error == nil else {
                    return
                }
                var result : Connector.weatherDataDec?

                do {
                     result = try? JSONDecoder().decode(weatherDataDec.self, from: data!)
                }
                    catch let jsonErr {
                        print("Error decoding getWeatherForCityList", jsonErr)
                        completion(false)
                    }
                
                if let resultList = result?.list {
                    for resultData in resultList {
                        
                        // saving current weather in city cycle to file, data divided by | sign for future parsing
                        let weatherString = "\(resultData.name)|\(Int(resultData.main.temp!))|\(Int(resultData.main.humidity!))|\(resultData.weather[0].description)|\(Int(resultData.main.pressure!))"
                        
                        self.saveHourlyWeather(city:String(describing:resultData.id), weather:weatherString)
                    }
                }
                let filename = self.getDocumentsDirectory().appendingPathComponent(Settings()._currentWeatherFile)
                do {
                    try data?.write(to: filename, options: .atomic)
                }
                catch{
                    print ("error saving local file getWeatherForCityList")
                }
                completion(true)

            })
            task.resume()
    }
    
    func fullCurrentPathToCity(city:String) -> String{
        
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy/MM/dd/HH"
        
        let fullDirPath = formatter.string(from: date)
        
        let fullPath = Settings()._mainWeatherDir+"/"+fullDirPath
        
        return fullPath
    }
    
    func saveHourlyWeather(city: String, weather: String){

        let wrPath = Settings()._mainWeatherDir+"/"+createDirForCurrentHour()+"/"
        let filename = self.getDocumentsDirectory().appendingPathComponent(wrPath+city+".json")
        
        do {
            try weather.write(to: filename, atomically: false, encoding: .utf8)
        }
        catch {
            print ("error saving local file saveHourlyWeather \(wrPath)test.json")
        }
        
    }
    
//    create directories for current hour
    func createDirForCurrentHour() -> String {

        let date = Date()
        let formatter = DateFormatter()

        formatter.dateFormat = "yyyy/MM/dd/HH"

        let fullDirPath = formatter.string(from: date)

        var city = Connector().getCitiesArray()

        for singleCity in city {
            if(singleCity != "0") {
                Connector.createDirectory(folderName:fullCurrentPathToCity(city:singleCity))
            }
        }
        return fullDirPath
    }

// get hourly weather for one city for all logged days
    func getHourlyForCity(city: Int, date: String)->[(
        cityId: Int,
        city: String,
        time: String,
        temp: String,
        pressure: String,
        humidity: String)]{
            
            let datePath = date+"/"

        // Documents/cityWeather/yyyy/MM/dd/
        let tempFolderPath = self.getDocumentsDirectory().path+"/"+Settings()._mainWeatherDir+"/"+datePath
        do {

            let filePaths = try self.fileManager.contentsOfDirectory(atPath: tempFolderPath)

            // cycling through files in current day dir
            for filePath in filePaths {
                
                // reading a file with given city id (e.g. 12345.json)
                let filename = self.getDocumentsDirectory().appendingPathComponent("/"+Settings()._mainWeatherDir+"/"+datePath+"/\(filePath)/\(city).json")
                let weatherContent = try String(contentsOf: filename);
                
                // explode content by | sign to array
                let weatherDataDay = weatherContent.components(separatedBy: "|")
                
                // applying content to existing array to return
                _arrays.weatherHourlyData.append((
                    cityId: city,
                    city: weatherDataDay[0],
                    time: filePath+":00",
                    temp: weatherDataDay[1],
                    pressure: weatherDataDay[4],
                    humidity: weatherDataDay[2]
                    ))
            }
        } catch {
            print("244 Could not read daily dir: \(error)")
        }
            _arrays.weatherHourlyData.sort{$0.time > $1.time}
            return _arrays.weatherHourlyData
    }

    
// get daily weather for one city for all logged days

    func getDailyForCity(city: Int)->[(
        cityId: Int,
        cityName: String,
        time: String?,
        day: String,
        date: String,
        temp: String,
        pressure: String,
        humidity: String,
        seaLevel: String,
        sunrise: String,
        sunset: String,
        wind: String
        )]{

            var tempDay : Int = 0
            var cityName : String = "--"
            var pressureDay : Int = 0
            var humDay : Int = 0
            
            let date = Date()
            let formatter = DateFormatter()
            

            // dir dates structure
            formatter.dateFormat = "yyyy/MM/"
            let datePath = formatter.string(from: date)

            

            // Documents/cityWeather/yyyy/MM/
            let tempFolderPath = self.getDocumentsDirectory().path+"/"+Settings()._mainWeatherDir+"/"+datePath
            do {

                let filePaths = try self.fileManager.contentsOfDirectory(atPath: tempFolderPath)

                // cycling through files in current day dir
                for filePath in filePaths {
                    
                    tempDay = 0
                    pressureDay = 0
                    humDay = 0
                    
                    let filePathsHours = try self.fileManager.contentsOfDirectory(atPath: tempFolderPath+"/"+filePath)
                    
                    // cycling through files in current day dir
                    for filePathHour in filePathsHours {
                        let filename = self.getDocumentsDirectory().appendingPathComponent("/"+Settings()._mainWeatherDir+"/"+datePath+"\(filePath)/\(filePathHour)/\(city).json")

                        let weatherContent = try String(contentsOf: filename);

                         let weatherDataDay = weatherContent.components(separatedBy: "|")

                        cityName = weatherDataDay[0]

                        tempDay = tempDay + Int(weatherDataDay[1])!
                        pressureDay = pressureDay + Int(weatherDataDay[4])!
                        humDay = humDay + Int(weatherDataDay[2])!
                    }

                    humDay = humDay / filePathsHours.count
                    pressureDay = pressureDay / filePathsHours.count
                    tempDay = tempDay / filePathsHours.count

                    _arrays.weatherDailyData.append((
                        cityId: city,
                        cityName: cityName,
                        time: "",
                        day: "",
                        date: datePath+filePath,
                        temp: String(describing:tempDay),
                        pressure: String(describing:pressureDay),
                        humidity: String(describing:humDay),
                        seaLevel: "",
                        sunrise: "",
                        sunset: "",
                        wind: ""
                    ))

                }
            } catch {
                print("------ Could not read daily dir: \(error)")
            }
            _arrays.weatherDailyData.sort{$0.date > $1.date}

            return _arrays.weatherDailyData
    }

    func findCityByName(city: String, completion: @escaping ([(String?, String, String?)])->()){

        let requestString = Settings().GET_CITY_BY_NAME+city
        print("req name: \(requestString)")

        let request = self.getRequest(urlAddress: requestString, method: "GET", sendParams: "" )
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil && error == nil else {
                return
            }

            let result = try? JSONDecoder().decode([searchCity].self, from: data!)
            
            let outputStr  = String(data: data!, encoding: String.Encoding.utf8) as String!
            
            if (result == nil){ // if different structure received
                print("search json doesn't fit: \(outputStr)")
                return
            }
            else{
                for resultData in result! {
                    self._arrays.foundCityNames.append((
                        resultData.name,
                        resultData.country!,
                        resultData.city_id
                    ))
                }

                completion(self._arrays.foundCityNames)
            }
        })
        task.resume()
    }
    
    func findCityByCoords(city: String, completion: @escaping ([(String?, String, String?)])->()){

            let coords = city.components(separatedBy: ",")
            let requestString = Settings().MAIN_API_URL+Settings().GET_CITY_BY_COORD+"lat=\(coords[0])&lon=\(coords[1])"+Settings().GET_WEATHER_APPID+Settings().API_KEY

        print("req coords: \(requestString)")
        let request = self.getRequest(urlAddress: requestString, method: "GET", sendParams: "" )
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil && error == nil else {
                return
            }
            
            let result = try? JSONDecoder().decode(searchCoords.self, from: data!)
            
            let outputStr  = String(data: data!, encoding: String.Encoding.utf8) as String!
            
            if (result == nil){ // if different structure received
                print("search json doesn't fit: \(outputStr)")
                return
            }
            else{
                self._arrays.foundCityNames.append((
                    result?.name,
                    "",
                    "\(result?.id ?? 0)"
                ))

                completion(self._arrays.foundCityNames)
            }
        })
        task.resume()
    }
    
//  save 0 as city id to the file if it's empty (in order to not cause error when saving another city and easier save another city
//  with comma at the begginning of the string (0,123,243,345 etc))
    func initCitiesFile(){
        if getCities() == "0" {
            saveCities(dataToWrite: "0", completion: {
                _ in
                print("weather file init")
            })
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/"
            let datePath = formatter.string(from: date)
            print("creating init dir: \(Settings()._mainWeatherDir+"/"+datePath)")
            Connector.createDirectory(folderName:Settings()._mainWeatherDir+"/"+datePath)
        }
    }

    func getHourlyFilename() -> URL{
        return self.getDocumentsDirectory().appendingPathComponent(Settings()._cityDayWeather)
    }

//  get filename with cities saved
    func getCitiesFilename() -> URL{
        return self.getDocumentsDirectory().appendingPathComponent(Settings()._selectedCities)
    }

//  save updated cities file
    func saveCities(dataToWrite: String, completion: @escaping (Bool)->()) {
        do {
            try dataToWrite.write(to: getCitiesFilename(), atomically: false, encoding: .utf8)
            completion(true)
        }
        catch{
            print ("error saving cities file")
            completion(false)
        }
        
    }

//  get saved cities as string
    func getCities() -> String{
        var citiesString : String = "0"
        do {
            citiesString = try String(contentsOf: getCitiesFilename(), encoding: .utf8)
        }
        catch {}
        print ("getCities: \(citiesString)")
        return citiesString
    }

//  get saved cities as an array
    func getCitiesArray() -> [(String)]{

        var citiesArr : [String] = []

        do {
             citiesArr = getCities().components(separatedBy: ",")
        }
        catch {}

        return citiesArr
    }

    func checkIfCityExists(city: String) -> Bool{
        if getCities().contains(city) {
            return true
        }
        return false
    }
    
    func saveCityFromGPS(city: String, completion: @escaping (Bool)->()) {

        Connector().findCityByCoords(city: city, completion:  {
            (data) in
                self._arrays.foundCityNames = data
            
                    if Connector().checkIfCityExists(city: self._arrays.foundCityNames[0].id!) {
                        print ("exists")
                        completion(true)
                    }
                    else {
                        print("doesn't exists")
                        
                        let citiesToWrite = Connector().getCities()+","+self._arrays.foundCityNames[0].id!
                        Connector().saveCities(dataToWrite: citiesToWrite, completion: {
                            _ in
                                completion(true)
                        })
                    }
            })
    }


//  create directory for weather files
    static func createDirectory(folderName: String) -> URL? {

        let fileManager = FileManager.default

        if let documentDirectory = fileManager.urls(for: .documentDirectory,
                                                    in: .userDomainMask).first {

            let folderURL = documentDirectory.appendingPathComponent(folderName)
            if !fileManager.fileExists(atPath: folderURL.path) {
                do {
                    try fileManager.createDirectory(atPath: folderURL.path,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
                } catch {
                    print("error create folder: \(folderURL.path)")
                    print(error.localizedDescription)
                    return nil
                }
            }

            return folderURL
        }
        
        return nil
    }

//  get documents directory
    func getDocumentsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

//  create http request
    func getRequest(urlAddress:String,method:String,sendParams:String)->NSMutableURLRequest{
        let request = NSMutableURLRequest(url: URL(string: urlAddress)!)
        request.httpMethod = method
        request.httpBody = sendParams.data(using: String.Encoding.utf8)
        return request
    }

}
