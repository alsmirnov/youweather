//
//  Settings.swift
//  WeatherApp
//
//  Created by funssoftware on 2/8/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit

class Settings: NSObject {
    
    let _currentWeatherFile = "_mainWeather.json"
    let _selectedCities = "_selectedCities.json"
    let _mainWeatherDir = "cityWeather"
    let _cityDayWeather = "_cityDayWeather.json"
    
    let API_KEY = "717f93d9b33d969a7366735d7cac4301"
    
    // for single city
    //    let GET_WEATHER_URL = "https://samples.openweathermap.org/data/2.5/weather?q="
    
    let MAIN_API_URL = "https://api.openweathermap.org/data/2.5/"
    
    let GET_WEATHER_URL = "group?id="
    let GET_WEATHER_ICON = "https://openweathermap.org/img/w/"
    let GET_WEATHER_APPID = "&units=metric&appid="
    let GET_CITY_BY_NAME = "http://boonexpert.com/owm/?city="
    let GET_CITY_BY_COORD = "weather?"

}
