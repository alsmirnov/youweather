//
//  Helper.swift
//  WeatherApp
//
//  Created by funssoftware on 2/6/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit

class Helper {
    
   
    func applyShadow(label: UILabel){
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowRadius = 1.0
        label.layer.shadowOpacity = 1.0
        label.layer.shadowOffset = CGSize(width: 1, height: 1)
        label.layer.masksToBounds = false
    }
    
    func isTimeToUpdate() ->Bool{
        
        // TODO: - check every full hour
        return true
    }
    
}

extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
}

extension UIImageView {
        
        func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
            contentMode = mode
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                DispatchQueue.main.async() {
                    self.image = image
                }
                }.resume()
        }
        func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
            guard let url = URL(string: link) else { return }
            downloaded(from: url, contentMode: mode)
        }
}


