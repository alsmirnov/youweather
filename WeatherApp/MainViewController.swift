//
//  MainViewController.swift
//  WeatherApp
//
//  Created by funssoftware on 2/6/19.
//  Copyright © 2019 funssoftware. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class WeatherCell: UITableViewCell {
    @IBOutlet var skyIcon: UIImageView!
    @IBOutlet var cityName: UILabel!
    @IBOutlet var skyLabel: UILabel!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var cellBackgrund: UIImageView!
    
    @IBOutlet var condition: UILabel!
}

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
   
    @IBOutlet var weatherTable: UITableView!

    @IBOutlet var naviBar: UINavigationItem!
    let locationManager = CLLocationManager()
    
    @IBOutlet var infoLabel: UILabel!
    let _arrays = Arrays()

    var cityTitle: String = ""
    var cityId: String = ""
    var weatherCity : String = "0"
    
    var timer: Timer?
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.infoLabel.text = "loading..."
        isAuthorizedtoGetUserLocation()
        weatherTable.isHidden = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        }

        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation();
        }

        self.weatherTable.addSubview(self.refreshControl)

        Connector().initCitiesFile()
        weatherCity = Connector().getCities()
        
        if(weatherCity != "0"){
            self.infoLabel.text = "getting weather..."
            getWeatherData()
            timerStart()
        }
        else {
           
        }
    }

    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            self.infoLabel.text = "requesting location"
            
            locationManager.requestWhenInUseAuthorization()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.infoLabel.text = "please wait..."
        let userLocation :CLLocation = locations[0] as CLLocation
        print("Location: \(userLocation.coordinate.latitude), \(userLocation.coordinate.longitude)")

            Connector().saveCityFromGPS(city: "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)", completion: {_ in
                 DispatchQueue.main.async {
                    self.getWeatherData()
                    self.infoLabel.text = "reloading table..."
                    self.weatherTable.reloadData()
                }
        })
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.infoLabel.text = "failed to get location"
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.infoLabel.isHidden = true
            self.weatherTable.isHidden = false
        }
    }

    func timerStart(){
        if self.timer == nil {
            self.infoLabel.text = "update timer started"
            self.timer = Timer.scheduledTimer(timeInterval: 1800, target: self, selector: #selector(self.timerUpdate), userInfo: nil, repeats: true)
        }
    }

    @objc func timerUpdate() {
        getWeatherData()
    }
    
    func getWeatherData(){
        self.infoLabel.text = "getting weather data..."
        Connector().getWeatherForCityList(citiesList: Connector().getCities(), completion:  {_ in
            self._arrays.weatherTableData = Connector().getWeatherData(city: self.weatherCity, completion:  {_ in
                DispatchQueue.main.async {
                    self.weatherTable.reloadData()
                    self.infoLabel.isHidden = true
                    self.weatherTable.isHidden = false
                }
            })
        })
    }

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        refreshControl.frame.size.width = 100
        refreshControl.frame.size.height = 100
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getWeatherData()
        refreshControl.endRefreshing()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("table count: \(_arrays.weatherTableData.count)")
        return _arrays.weatherTableData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = weatherTable.dequeueReusableCell(withIdentifier: "WeatherCell") as! WeatherCell
        
        Helper().applyShadow(label:cell.tempLabel)
        Helper().applyShadow(label:cell.cityName)
        Helper().applyShadow(label:cell.skyLabel)
        
        cell.cellBackgrund.image = UIImage(named: _arrays.weatherTableData[indexPath.row].icon)
        cell.cityName.text = _arrays.weatherTableData[indexPath.row].city
        cell.tempLabel.text = "\(_arrays.weatherTableData[indexPath.row].temp)°"
        cell.skyLabel.text = _arrays.weatherTableData[indexPath.row].sky
        cell.skyIcon.downloaded(from: Settings().GET_WEATHER_ICON+_arrays.weatherTableData[indexPath.row].icon+".png")

        cell.condition.text = _arrays.weatherTableData[indexPath.row].icon
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.performSegue(withIdentifier: "dailyWeatherSegue", sender: self)
        self.weatherTable.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    @IBAction func unwindToTaskEditViewController(segue: UIStoryboardSegue) {
        
        let source = segue.source as? SearchViewController
        
        cityTitle = (source?.cityTitleUnwind)!
        cityId = (source?.cityIdUnwind)!

        getWeatherData()
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "dailyWeatherSegue",
            let destination = segue.destination as? DailyWeatherViewController,
            let selIndex = self.weatherTable.indexPathForSelectedRow?.row
        {
            destination.cityId = _arrays.weatherTableData[selIndex].cityId
            destination.tempNow = String(describing:_arrays.weatherTableData[selIndex].temp)
        }

    }
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 

}
